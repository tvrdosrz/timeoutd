CFLAGS = -g -Wall

timeoutd:	timeoutd.c
	$(CC) $(CFLAGS) -o timeoutd.o -c timeoutd.c -DTIMEOUTDX11
	$(CC) $(CFLAGS) -o timeoutd timeoutd.o -lX11 -lXss

clean:
	-rm -f timeoutd timeoutd.o

install:	timeoutd /etc/timeouts
	install timeoutd /usr/sbin/timeoutd
	install timeoutd.8 /usr/share/man/man8
	install timeouts.5 /usr/share/man/man5

uninstall:
	rm /usr/sbin/timeoutd
	rm /usr/share/man/man8/timeoutd.8
	rm /usr/share/man/man5/timeouts.5
	rm /etc/timeouts

/etc/timeouts:
	install timeouts /etc
